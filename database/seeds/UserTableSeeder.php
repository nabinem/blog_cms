<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'Nabin',
                'email' => 'nabinem_yaktumba@yahoo.com',
                'password' => bcrypt('123456')
            ],
            [
                'name' => 'Sudan',
                'email' => 'anonym_nep@yahoo.com',
                'password' => bcrypt('123456')  
            ]
        ]);
    }
}
