<?php

$formArray = [];

if (isset($_POST['project-name']) && trim($_POST['project-name']) != "") {
    $formArray['project-name'] = $_POST['project-name'];
} else {
    $formArray['project-name'] = "No data available.";
}
if (isset($_POST['full-name']) && trim($_POST['full-name']) != "") {
    $formArray['full-name'] = $_POST['full-name'];
} else {
    $formArray['full-name'] = "No data available.";
}
if (isset($_POST['email']) && trim($_POST['email']) != "") {
    $formArray['email'] = $_POST['email'];
} else {
    $formArray['email'] = "No data available.";
}
if (isset($_POST['phone-number']) && trim($_POST['phone-number']) != "") {
    $formArray['phone-number'] = $_POST['phone-number'];
} else {
    $formArray['phone-number'] = "No data available.";
}
if (isset($_POST['business-background']) && trim($_POST['business-background']) != "") {
    $formArray['business-background'] = $_POST['business-background'];
} else {
    $formArray['business-background'] = "No data available.";
}
if (isset($_POST['audience-primary']) && trim($_POST['audience-primary']) != "") {
    $formArray['audience-primary'] = $_POST['audience-primary'];
} else {
    $formArray['audience-primary'] = "No data available.";
}
if (isset($_POST['audience-seconday']) && trim($_POST['audience-seconday']) != "") {
    $formArray['audience-seconday'] = $_POST['audience-seconday'];
} else {
    $formArray['audience-seconday'] = "No data available.";
}
if (isset($_POST['objectives']) && trim($_POST['objectives']) != "") {
    $formArray['objectives'] = $_POST['objectives'];
} else {
    $formArray['objectives'] = "No data available.";
}
if (isset($_POST['the-message']) && trim($_POST['the-message']) != "") {
    $formArray['the-message'] = $_POST['the-message'];
} else {
    $formArray['the-message'] = "No data available.";
}
if (isset($_POST['design-information-1']) && trim($_POST['design-information-1']) != "") {
    $formArray['design-information-1'] = $_POST['design-information-1'];
} else {
    $formArray['design-information-1'] = "No data available.";
}
if (isset($_POST['design-information-2']) && trim($_POST['design-information-2']) != "") {
    $formArray['design-information-2'] = $_POST['design-information-2'];
} else {
    $formArray['design-information-2'] = "No data available.";
}
if (isset($_POST['design-requirements']) && trim($_POST['design-requirements']) != "") {
    $formArray['design-requirements'] = $_POST['design-requirements'];
} else {
    $formArray['design-requirements'] = "No data available.";
}
if (isset($_POST['other-requirements']) && trim($_POST['other-requirements']) != "") {
    $formArray['other-requirements'] = $_POST['other-requirements'];
} else {
    $formArray['other-requirements'] = "No data available.";
}
if (isset($_POST['deadline']) && trim($_POST['deadline']) != "") {
    $formArray['deadline'] = $_POST['deadline'];
} else {
    $formArray['deadline'] = "No data available.";
}
if (isset($_POST['responsible-parties']) && trim($_POST['responsible-parties']) != "") {
    $formArray['responsible-parties'] = $_POST['responsible-parties'];
} else {
    $formArray['responsible-parties'] = "No data available.";
}

$emailArray = [];
$emailArray['to'] = "hello@natoof.ae";
$emailArray['subject'] = "New Creative Brief";
$emailArray['headers'] = "MIME-Version: 1.0 \r\n";
$emailArray['headers'] .= "Content-type:text/html;charset=UTF-8 \r\n";
$emailArray['headers'] .= "From: Natoof Website <no-reply@natoof.ae> \r\n";
$emailArray['headers'] .= "Reply-to: " . $formArray['full-name'] . "<" . $formArray['email'] . "> \r\n";
$emailArray['message'] = "<h1>New Creative Brief</h1><p>A new creative brief has been submitted by the Natoof website!</p>";
$emailArray['message'] .= "<h2>Project name</h2><p>" . $formArray['project-name'] . "</p>";
$emailArray['message'] .= "<h2>Contact information</h2>";
$emailArray['message'] .= "<h3>Full name</h3><p>" . $formArray['full-name'] . "</p>";
$emailArray['message'] .= "<h3>Email</h3><p>" . $formArray['email'] . "</p>";
$emailArray['message'] .= "<h3>Phone number</h3><p>" . $formArray['phone-number'] . "</p>";
$emailArray['message'] .= "<h2>Business background</h2><p>" . $formArray['business-background'] . "</p>";
$emailArray['message'] .= "<h2>Audience</h2>";
$emailArray['message'] .= "<h3>Primary audience</h3><p>" . $formArray['audience-primary'] . "</p>";
$emailArray['message'] .= "<h3>Secondary audience</h3><p>" . $formArray['audience-seconday'] . "</p>";
$emailArray['message'] .= "<h2>Objectives</h2><p>" . $formArray['objectives'] . "</p>";
$emailArray['message'] .= "<h2>The message</h2><p>" . $formArray['the-message'] . "</p>";
$emailArray['message'] .= "<h2>Design information</h2>";
$emailArray['message'] .= "<h3>Details of the logo</h3><p>" . $formArray['design-information-1'] . "</p>";
$emailArray['message'] .= "<h3>Correct wording</h3><p>" . $formArray['design-information-2'] . "</p>";
$emailArray['message'] .= "<h2>Design requirements</h2><p>" . $formArray['design-requirements'] . "</p>";
$emailArray['message'] .= "<h2>Other information</h2><p>" . $formArray['other-requirements'] . "</p>";
$emailArray['message'] .= "<h2>Deadline</h2><p>" . $formArray['deadline'] . "</p>";
$emailArray['message'] .= "<h2>Responsible parties</h2><p>" . $formArray['responsible-parties'] . "</p>";

mail($emailArray['to'], $emailArray['subject'], $emailArray['message'], $emailArray['headers']);
?>