<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>@yield('title') - My Blog</title>
        <link href="{{ theme('css/frontend.css') }}" rel="stylesheet">
    </head>
    <body>
        <nav class="nav navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand">
                        <img src="{{ theme('images/logo.png') }}" alt="my blog">
                    </a>
                </div>
                <ul class="nav navbar-nav">
                    @include('partials.navigation')
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>