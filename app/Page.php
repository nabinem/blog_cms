<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Page extends Node
{
    protected $fillable = ['title', 'name', 'url', 'content', 'template', 'hidden'];
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value ?: null;
    }
    
    public function setTemplateAttribute($value)
    {
        $this->attributes['template'] = $value ?: null;
    }
    
    public function updateOrder($order, $orderPage)
    {
        $orderPage = $this->findOrFail($orderPage);
        
        if ($order == 'before'){
            $this->moveToLeftOf($orderPage);
        } elseif ($order == 'after') {
            $this->moveToRightOf($orderPage);
        } elseif($order == 'childof'){
            $this->makeChildOf($orderPage);
        }
    }
    
}
